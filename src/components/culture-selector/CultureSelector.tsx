import styles from './culture-selector.module.scss';

export const CultureSelector = () => {
  return <div className={styles.container}>
    <div>English</div>
    <div>USD - $</div>
  </div>;
};
